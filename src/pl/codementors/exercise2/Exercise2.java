package pl.codementors.exercise2;
import java.util.Scanner;

public class Exercise2{

	public static void main(String[] args){

		System.out.println("Zadanie nr 2");
		System.out.println("Podaj rozmiar tablicy");

		Scanner inputScanner = new Scanner(System.in);
		int a = inputScanner.nextInt();
		int[] array = new int [a];


	
		for(int i = 0; i < array.length; i++){
			System.out.println("Podaj element nr " + (i + 1));
			array[i] = inputScanner.nextInt();
		}


		int min = array[0];
		int max = array[0];

		for(int i = 1; i < array.length; i++){
			if(array[i] < min){
				min = array[i];
			}

			if(array[i] > max){
				max = array[i];
			}	
		}

		System.out.println("Najmniejszy element tablicy wynosi: " + min);
		System.out.println("Najwiekszy element tablicy wynosi: " + max);
	}

}
