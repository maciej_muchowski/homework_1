package pl.codementors.exercise3;
import java.util.Scanner;

public class Exercise3{

	public static void main(String[] args){

		System.out.println("Zadanie nr 3");

		Scanner inputScanner = new Scanner(System.in);
		int decimalNumber;
		
		System.out.println("Podaj liczbe z zakresu 0-15");
		decimalNumber = inputScanner.nextInt();

		if(decimalNumber < 0 || decimalNumber > 15){
			System.out.println("Podana liczba wykracza poza zakres 0-15");
		} else{
			int binaryNumber[] = new int[4];
			int i = 3;
			
			do{
				binaryNumber[i] = decimalNumber % 2 ;
				decimalNumber = decimalNumber / 2;
				i--;
			}while(i >= 0);
			
			System.out.print("Podana liczba w postaci binarnej to: ");
			for(i = 0; i < binaryNumber.length; i++){
				System.out.print(binaryNumber[i]);
			}
		System.out.println();
		}
	}

}
